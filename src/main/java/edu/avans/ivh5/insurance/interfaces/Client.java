package edu.avans.ivh5.insurance.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Client extends Remote {

    /**
     * gets id
     *
     * @return id
     */
    public int getId() throws RemoteException;

}
