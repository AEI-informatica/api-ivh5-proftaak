package edu.avans.ivh5.insurance.interfaces;

import java.rmi.RemoteException;

/**
 *
 * @author Mathias
 */
public interface InsuranceIF {
    
    /**
     *
     * @param bsn
     * @return Client
     * @throws Exception
     * @throws RemoteException
     */
    public Client getClient(String bsn) throws RemoteException;
}
