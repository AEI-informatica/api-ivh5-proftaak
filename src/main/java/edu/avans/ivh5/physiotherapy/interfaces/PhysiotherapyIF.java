package edu.avans.ivh5.physiotherapy.interfaces;

import java.rmi.RemoteException;
import java.util.HashMap;

/**
 *
 * @author Anh-Duc
 */
public interface PhysiotherapyIF {

    /**
     *
     * @return HashMap<Integer,Treatment>
     * @throws RemoteException
     */
    HashMap<Integer, Treatment> getTreatments() throws RemoteException;
}
