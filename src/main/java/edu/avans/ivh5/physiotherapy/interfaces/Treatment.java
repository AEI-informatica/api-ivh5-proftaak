package edu.avans.ivh5.physiotherapy.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

public interface Treatment extends Remote {

    /**
     * creates treatment object
     *
     * @param id of a treatment
     */
    // public Treatment(int id) throws RemoteException;

    /**
     * gets id
     *
     * @return id
     */
    public int getId() throws RemoteException;

    /**
     * gets physiotherapistID
     *
     * @return physiotherapistID
     */
    public int getPhysiotherapistID() throws RemoteException;

    /**
     * gets clientBSN
     *
     * @return clientBSN
     */
    public String getClientBSN() throws RemoteException;

    /**
     * gets arraylist of treatmentcodes
     *
     * @return arraylist of treatmentcodes
     */
    public ArrayList<TreatmentCode> getTreatmentcodes() throws RemoteException;

    /**
     * get doubel totalprice
     *
     * @return doubel totalprice
     */
    public double getTotalPrice() throws RemoteException;

    /**
     * sets physiotherapist id
     *
     * @param physiotherapistID the id of a physiotherapist
     */
    public void setPhysiotherapistID(int physiotherapistID) throws RemoteException;

    /**
     * sets clientbsn
     *
     * @param clientBSN the bsn of a client
     */
    public void setClientBSN(String clientBSN) throws RemoteException;

    /**
     * adds treatmentcode
     *
     * @param treatmentCode
     */
    public void addTreatmentCode(TreatmentCode treatmentCode) throws RemoteException;

    /**
     * removestreatmentcode
     *
     * @param treatmentCode
     */
    public void removeTreatmentCode(TreatmentCode treatmentCode) throws RemoteException;

    /**
     * calculates the start date of the treatment
     *
     * @return Date
     */
    public Date getStartDate() throws RemoteException;

    /**
     * calculates the start date of the treatment
     *
     * @return Date
     */
    public Date getEndDate() throws RemoteException;

}
