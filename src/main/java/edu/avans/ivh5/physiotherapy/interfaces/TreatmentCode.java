package edu.avans.ivh5.physiotherapy.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TreatmentCode extends Remote {

    /**
     * gets id
     *
     * @return id
     */
    public int getId() throws RemoteException;

}
